<?php

namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ItemRepository")
 * @ORM\Table(name="items", indexes={@Index(name="item_idx", columns={"name"})})
 * @package AppBundle\Entity
 */
class Item
{

    /**
     * @ORM\Column(type="integer", unique=true, nullable=false)
     * @ORM\Id()
     * @var int $id
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @var string $name
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string $addName
     */
    private $addName;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string $description
     */
    private $description;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string $icon
     */
    private $icon;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Drop", mappedBy="item", cascade={"persist", "remove"})
     * @var ArrayCollection $drops
     */
    private $drops;

    /**
     * Item constructor.
     */
    public function __construct()
    {
        $this->drops = new ArrayCollection();
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAddName()
    {
        return $this->addName;
    }

    /**
     * @param string $addName
     */
    public function setAddName($addName)
    {
        $this->addName = $addName;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    /**
     * @return ArrayCollection
     */
    public function getDrops()
    {
        return $this->drops;
    }

    /**
     * @param ArrayCollection $drops
     */
    public function setDrops($drops)
    {
        $this->drops = $drops;
    }

    /**
     * @param Drop $drop
     */
    public function addDrop($drop)
    {
        $this->drops->add($drop);
    }
}