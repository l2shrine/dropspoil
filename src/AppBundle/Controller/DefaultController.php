<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Drop;
use AppBundle\Entity\Item;
use AppBundle\Entity\Npc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="home")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $items = [];
        $npcs = [];

        $form = $this->createFormBuilder()
            ->setMethod('get')
            ->add('term', TextType::class, ['label' => 'Search database for', 'required' => true])
            ->add('search', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $term = $data['term'];

            $items = $this->getDoctrine()->getRepository(Item::class)->findByName($term);
            $npcs = $this->getDoctrine()->getRepository(Npc::class)->findByName($term);
        }

        return $this->render(
            'default/index.html.twig',
            [
                'form' => $form->createView(),
                'items' => $items,
                'npcs' => $npcs,
            ]
        );
    }

    /**
     * @Route("/item/{id}", name="item")
     * @param Request $request
     * @return Response
     */
    public function itemAction(Request $request, $id)
    {
        $item = $this->getDoctrine()->getRepository(Item::class)->find($id);
        $drops = $this->getDoctrine()->getRepository(Drop::class)->findAllByItemId($id);

        if ($item == null) {
            throw new NotFoundHttpException();
        }

        $items = [];
        $spoil = [];

        foreach ($drops as $drop) {
            $dropType = $drop->getType();

            switch ($dropType) {
                case 217:
                    $items[] = $drop;
                    break;
                case 144:
                    $spoil[] = $drop;
                    break;
            }
        }

        return $this->render(':default:item.html.twig', ['item' => $item, 'items' => $items, 'spoil' => $spoil]);
    }

    /**
     * @Route("/npc/{id}", name="npc")
     * @param Request $request
     * @return Response
     */
    public function npcAction(Request $request, $id)
    {
        $npc = $this->getDoctrine()->getRepository(Npc::class)->find($id);
        $drops = $this->getDoctrine()->getRepository(Drop::class)->findAllByNpcId($id);

        if ($npc == null) {
            throw new NotFoundHttpException();
        }

        $items = [];
        $spoil = [];
        $herbs = [];

        foreach ($drops as $drop) {
            $dropType = $drop->getType();

            switch ($dropType) {
                case 217:
                    $items[] = $drop;
                    break;
                case 144:
                    $spoil[] = $drop;
                    break;
                case 250:
                    $herbs[] = $drop;
                    break;
            }
        }

        return $this->render(':default:npc.html.twig', ['npc' => $npc, 'items' => $items, 'spoil' => $spoil, 'herbs' => $herbs]);
    }

    /**
     * @Route("/npc/loc/{id}", name="npc_loc")
     * @param Request $request
     * @return Response
     */
    public function npcLocationAction(Request $request, $id)
    {
        $npc = $this->getDoctrine()->getRepository(Npc::class)->find($id);

        if ($npc == null) {
            throw new NotFoundHttpException();
        }

        $color = new \ImagickPixel('rgb(255, 51, 51)');
        $draw = new \ImagickDraw();
        $draw->setStrokeColor($color);
        $draw->setStrokeWidth(1);

        foreach ($npc->getPositions() as $position) {
            $x = ceil(((intval($position->getX()) / 402) + (656 / 2)));
            $y = ceil(((intval($position->getY()) / 396) + (1310 / 2)));
            $draw->line($x + 3, $y, $x - 3, $y);
            $draw->line($x, $y + 3, $x, $y - 3);
        }

        $imagick = new \Imagick();
        $imagick->newImage(906, 1310, 'none');
        $imagick->setImageFormat('png');
        $imagick->drawImage($draw);
        $data = base64_encode($imagick->getImageBlob());

        return $this->render(':default:npc_location.html.twig', ['npc' => $npc, 'data' => $data]);
    }
}
