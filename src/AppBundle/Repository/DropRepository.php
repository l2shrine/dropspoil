<?php

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class DropRepository extends EntityRepository
{

    public function findAllByItemId($id)
    {
        $qb = $this->createQueryBuilder('d');

        return $qb->select(['d', 'n'])
            ->join('d.npc', 'n')
            ->where('d.item = :id')
            ->orderBy('n.name')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }

    public function findAllByNpcId($id)
    {
        $qb = $this->createQueryBuilder('d');

        return $qb->select(['d', 'n'])
            ->join('d.npc', 'n')
            ->where('d.npc = :id')
            ->orderBy('n.name')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }

}